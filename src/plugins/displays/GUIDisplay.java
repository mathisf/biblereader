package plugins.displays;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import application.interfaces.IAction;
import application.interfaces.IDisplay;
import application.models.Building;
import application.models.Person;
import application.models.Thing;

public class GUIDisplay implements IDisplay {
	private JFrame frame;
	private JComboBox<Building> buildingsComboBox;
	private Building selectedBuilding;
	private JComboBox<IAction> actionsComboBox;
	private IAction selectedAction;
	private JComboBox<Thing> thingsComboBox;
	private ThingComboBoxModel buildingThings;
	private Thing selectedThing;
	private JLabel text;
	private JButton doButton;
	private List<Person> persons;

	public void displayPerson(Person p) {
		this.frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		text = new JLabel(p.toString());
		text.setBounds(0, 0, 400, 50);

		frame.add(text);
		frame.setSize(400, 500);// 400 width and 500 height
		frame.setLayout(null);// using no layout managers
		frame.setVisible(true);// making the frame visible
	}

	public void displayAll(List<Person> persons, List<Building> buildings, List<IAction> actions) {
		frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		this.persons = persons;
		text = new JLabel(this.persons.get(0).toString());
		text.setBounds(0, 0, 200, 200);
		frame.add(text);

		buildingsComboBox = new JComboBox<Building>(new BuildingComboBoxModel(buildings));
		buildingsComboBox.setSelectedIndex(0);
		buildingsComboBox.setBounds(0, 200, 200, 50);
		buildingsComboBox.addActionListener(new BuildingThingsAction());
		frame.add(buildingsComboBox);

		actionsComboBox = new JComboBox<IAction>(new ActionComboBoxModel(actions));
		actionsComboBox.setSelectedIndex(0);
		actionsComboBox.setBounds(200, 0, 200, 50);
		frame.add(actionsComboBox);

		buildingThings = new ThingComboBoxModel(new ArrayList<Thing>(buildings.get(0).getContent()));

		thingsComboBox = new JComboBox<Thing>(buildingThings);
		thingsComboBox.setBounds(0, 250, 200, 50);
		frame.add(thingsComboBox);

		doButton = new JButton("Do");
		doButton.setBounds(200, 50, 200, 50);
		doButton.addActionListener(new DoAction());
		frame.add(doButton);

		frame.setSize(500, 500);
		frame.setLayout(null);
		frame.setVisible(true);
	}

	class ActionComboBoxModel extends AbstractListModel<IAction> implements ComboBoxModel<IAction> {
		private static final long serialVersionUID = 1L;
		List<IAction> actions = null;

		public ActionComboBoxModel(List<IAction> actions) {
			super();
			this.actions = actions;
		}

		public IAction getElementAt(int index) {
			return actions.get(index);
		}

		public int getSize() {
			return actions.size();
		}

		public void setSelectedItem(Object anItem) {
			selectedAction = (IAction) anItem;
		}

		// Methods implemented from the interface ComboBoxModel
		public Object getSelectedItem() {
			return selectedAction; // to add the selection to the combo box
		}
	}

	class ThingComboBoxModel extends AbstractListModel<Thing> implements ComboBoxModel<Thing> {
		private static final long serialVersionUID = 1L;
		List<Thing> things = null;

		public ThingComboBoxModel(List<Thing> things) {
			super();
			this.things = things;
		}

		public ThingComboBoxModel() {
			super();
		}

		public void setThings(List<Thing> things) {
			this.things = things;
		}

		public Thing getElementAt(int index) {
			return things.get(index);
		}

		public int getSize() {
			return things.size();
		}

		public void setSelectedItem(Object anItem) {
			selectedThing = (Thing) anItem;
		}

		// Methods implemented from the interface ComboBoxModel
		public Object getSelectedItem() {
			return selectedThing; // to add the selection to the combo box
		}
	}

	class BuildingComboBoxModel extends AbstractListModel<Building> implements ComboBoxModel<Building> {
		private static final long serialVersionUID = 1L;
		List<Building> buildings = null;

		public BuildingComboBoxModel(List<Building> buildings) {
			super();
			this.buildings = buildings;
		}

		public BuildingComboBoxModel() {
			super();
		}

		public void setBuildings(List<Building> buildings) {
			this.buildings = buildings;
		}

		public Building getElementAt(int index) {
			return buildings.get(index);
		}

		public int getSize() {
			return buildings.size();
		}

		public void setSelectedItem(Object anItem) {
			selectedBuilding = (Building) anItem;
		}

		public Object getSelectedItem() {
			return selectedBuilding; // to add the selection to the combo box
		}
	}

	class DoAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			List<Thing> things = new ArrayList<Thing>();
			things.add(selectedThing);
			selectedAction.doAction(persons, things);
			text.setText(persons.get(0).toString());
		}
	}

	class BuildingThingsAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			buildingThings.setThings(selectedBuilding.getContent());
		}
	}
}
