package plugins.factories;

import java.util.ArrayList;
import java.util.List;

import application.interfaces.IThingFactory;
import application.models.Building;
import application.models.Consumable;
import application.models.Thing;

public class BuildingFactory implements IThingFactory {
    public List<Thing> create() {
        List<Thing> things = new ArrayList<Thing>();
        List<Thing> churchThings = new ArrayList<Thing>();
        List<Thing> houseThings = new ArrayList<Thing>();
        Building house = new Building("House", 100, 100, -100, -100, 100);
        Building church = new Building("Church", 100, 100, 100, 100, 100);

        churchThings.add(new Thing("Bible - Old Testament", "Book"));
        churchThings.add(new Thing("Bible - New Testament", "Book"));
        houseThings.add(new Consumable("Bread", "Food", 0, 50, 5));
        houseThings.add(new Consumable("Bread", "Food", 0, 50, 5));
        churchThings.add(new Consumable("Wafer", "Food", 0, 5, 0));
        churchThings.add(new Consumable("Wafer", "Food", 0, 5, 0));
        churchThings.add(new Consumable("Wine", "Drink", 25, 0, -5));
        churchThings.add(new Consumable("Wine", "Drink", 25, 0, -5));
        houseThings.add(new Consumable("Water", "Drink", 25, 0, -5));
        houseThings.add(new Consumable("Water", "Drink", 25, 0, -5));

        house.setContent(houseThings);
        church.setContent(churchThings);

        things.add(house);
        things.add(church);

        return things;

    }
}