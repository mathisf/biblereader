package plugins.factories;

import java.util.ArrayList;
import java.util.List;

import application.interfaces.IPersonFactory;
import application.models.Person;

public class PersonFactory implements IPersonFactory {
    public List<application.models.Person> create() {
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("Jésus", "Christ", 2019));
        persons.add(new Person("Abaddon", "", 1));

        return persons;
    }
}