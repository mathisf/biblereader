package application.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Person {
	private String firstName;
	private String lastName;
	private int age;
	private int coordinateX;
	private int coordinateY;
	private int coordinateZ;
	private List<Buyable> possessions;
	private int money;
	private int hunger;
	private int thirst;
	private int energy;
	private int faith;

	public Person(String firstName, String lastName, int age, int coordinateX, int coordinateY, int coordinateZ,
			List<Buyable> possessions, int money, int hunger, int thirst, int energy, int faith) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.coordinateX = coordinateX;
		this.coordinateY = coordinateY;
		this.coordinateZ = coordinateZ;
		this.possessions = possessions;
		this.money = money;
		this.hunger = hunger;
		this.thirst = thirst;
		this.energy = energy;
		this.faith = faith;
	}

	public Person(String firstName, String lastName, int age) {
		this(firstName, lastName, age, 0, 0, 0, new ArrayList<Buyable>(), 0, 0, 0, 100, 0);
	}

	public Person() {
		this("", "", 0, 0, 0, 0, new ArrayList<Buyable>(), 0, 0, 0, 100, 0);
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getCoordinateX() {
		return this.coordinateX;
	}

	public void setCoordinateX(int coordinateX) {
		this.coordinateX = coordinateX;
	}

	public int getCoordinatey() {
		return this.coordinateY;
	}

	public void setCoordinatey(int coordinateY) {
		this.coordinateY = coordinateY;
	}

	public int getCoordinateZ() {
		return this.coordinateZ;
	}

	public void setCoordinateZ(int coordinateZ) {
		this.coordinateZ = coordinateZ;
	}

	public List<Buyable> getPossessions() {
		return this.possessions;
	}

	public void setPossessions(List<Buyable> possessions) {
		this.possessions = possessions;
	}

	public int getMoney() {
		return this.money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getHunger() {
		return this.hunger;
	}

	public void setHunger(int hunger) {
		if (hunger < 0) {
			this.hunger = 0;
			return;
		}

		if (hunger > 100) {
			this.hunger = 100;
			return;
		}
		this.hunger = hunger;
	}

	public int getThirst() {
		return this.thirst;
	}

	public void setThirst(int thirst) {
		if (thirst < 0) {
			this.thirst = 0;
			return;
		}

		if (thirst > 100) {
			this.thirst = 100;
			return;
		}
		this.thirst = thirst;
	}

	public int getEnergy() {
		return this.energy;
	}

	public void setEnergy(int energy) {
		if (energy < 0) {
			this.energy = 0;
			return;
		}

		if (energy > 100) {
			this.energy = 100;
			return;
		}
		this.energy = energy;
	}

	public int getFaith() {
		return this.faith;
	}

	public void setFaith(int faith) {
		if (faith < 0) {
			this.faith = 0;
			return;
		}

		if (faith > 100) {
			this.faith = 100;
			return;
		}
		this.faith = faith;
	}

	public Person firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public Person lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public Person age(int age) {
		this.age = age;
		return this;
	}

	public Person coordinateX(int coordinateX) {
		this.coordinateX = coordinateX;
		return this;
	}

	public Person coordinateY(int coordinateY) {
		this.coordinateY = coordinateY;
		return this;
	}

	public Person coordinateZ(int coordinateZ) {
		this.coordinateZ = coordinateZ;
		return this;
	}

	public Person possessions(List<Buyable> possessions) {
		this.possessions = possessions;
		return this;
	}

	public Person money(int money) {
		this.money = money;
		return this;
	}

	public Person hunger(int hunger) {
		if (hunger < 0) {
			this.hunger = 0;
		}

		if (hunger > 100) {
			this.hunger = 100;
		}

		this.hunger = hunger;
		return this;
	}

	public Person thirst(int thirst) {
		if (thirst < 0) {
			this.thirst = 0;
		}

		if (thirst > 100) {
			this.thirst = 100;
		}
		this.thirst = thirst;
		return this;
	}

	public Person energy(int energy) {
		if (energy < 0) {
			this.energy = 0;
		}

		if (energy > 100) {
			this.energy = 100;
		}
		this.energy = energy;
		return this;
	}

	public Person faith(int faith) {
		if (faith < 0) {
			this.faith = 0;
		}

		if (faith > 100) {
			this.faith = 100;
		}
		this.faith = faith;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof Person)) {
			return false;
		}
		Person person = (Person) o;
		return Objects.equals(firstName, person.firstName) && Objects.equals(lastName, person.lastName)
				&& age == person.age && coordinateX == person.coordinateX && coordinateY == person.coordinateY
				&& coordinateZ == person.coordinateZ && Objects.equals(possessions, person.possessions)
				&& money == person.money && hunger == person.hunger && thirst == person.thirst
				&& energy == person.energy && faith == person.faith;
	}

	@Override
	public int hashCode() {
		return Objects.hash(firstName, lastName, age, coordinateX, coordinateY, coordinateZ, possessions, money, hunger,
				thirst, energy, faith);
	}

	@Override
	public String toString() {
		return "<html>" + getFirstName() + " " + getLastName() + "<br/>&nbsp;0<br/>/|\\<br/>&nbsp;|<br/>/\\"
				+ "<br/>energy : " + getEnergy() + "<br/>" + "hunger : " + getHunger() + "" + "<br/>thirst : "
				+ getThirst() + "<br/>" + "faith : " + getFaith() + "<br/>" + "</html>";
	}
}
