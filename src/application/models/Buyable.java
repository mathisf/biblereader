package application.models;

public abstract class Buyable {
    protected int price;

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Buyable price(int price) {
        this.price = price;
        return this;
    }
}