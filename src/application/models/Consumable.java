package application.models;

import java.util.Objects;

public class Consumable extends Thing {
    private int drink;
    private int food;
    private int energy;

    public Consumable(String name, String type, int drink, int food, int energy, int price) {
        this.name = name;
        this.type = type;
        this.drink = drink;
        this.food = food;
        this.energy = energy;
        this.price = price;
    }

    public Consumable(String name, String type, int drink, int food, int energy) {
        this(name, type, drink, food, energy, 0);
    }

    public Consumable() {
        this("", "", 0, 0, 0, 0);
    }

    public void consume() {
        this.drink = 0;
        this.food = 0;
        this.energy = 0;
        this.name = this.name + " (consumed)";
    }

    public int getDrink() {
        return this.drink;
    }

    public void setDrink(int drink) {
        this.drink = drink;
    }

    public int getFood() {
        return this.food;
    }

    public void setFood(int food) {
        this.food = food;
    }

    public int getEnergy() {
        return this.energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public Consumable drink(int drink) {
        this.drink = drink;
        return this;
    }

    public Consumable food(int food) {
        this.food = food;
        return this;
    }

    public Consumable energy(int energy) {
        this.energy = energy;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Consumable)) {
            return false;
        }
        Consumable consumable = (Consumable) o;
        return drink == consumable.drink && food == consumable.food && energy == consumable.energy;
    }

    @Override
    public int hashCode() {
        return Objects.hash(drink, food, energy);
    }
}