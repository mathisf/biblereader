package application.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Building extends Thing {
    private int surface;
    private int coordinateX;
    private int coordinateY;
    private int width;
    private int length;
    private List<Thing> content;

    public Building(String name, int surface, int coordinateX, int coordinateY, int width, int length, int price) {
        this.name = name;
        this.type = "Building";
        this.surface = surface;
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.width = width;
        this.length = length;
        this.price = price;
        this.content = new ArrayList<Thing>();
    }

    public Building(String name, int surface, int coordinateX, int coordinateY, int width, int length) {
        this(name, surface, coordinateX, coordinateY, width, length, 0);
    }

    public Building() {
        this("name", 0, 0, 0, 0, 0, 0);
    }

    public int getSurface() {
        return this.surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public int getCoordinateX() {
        return this.coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return this.coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<Thing> getContent() {
        return this.content;
    }

    public void setContent(List<Thing> content) {
        this.content = content;
    }

    public void addThingIntoContent(Thing thing) {
        this.content.add(thing);
    }

    public Building surface(int surface) {
        this.surface = surface;
        return this;
    }

    public Building coordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
        return this;
    }

    public Building coordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
        return this;
    }

    public Building width(int width) {
        this.width = width;
        return this;
    }

    public Building length(int length) {
        this.length = length;
        return this;
    }

    public Building content(List<Thing> content) {
        this.content = content;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Building)) {
            return false;
        }
        Building building = (Building) o;
        return Objects.equals(name, building.getName()) && Objects.equals(type, building.getType())
                && surface == building.getSurface() && coordinateX == building.getCoordinateX()
                && coordinateY == building.getCoordinateY() && width == building.getWidth()
                && length == building.getLength();
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, surface, coordinateX, coordinateY, width, length);
    }

    @Override
    public String toString() {
        return getName();
    }

}