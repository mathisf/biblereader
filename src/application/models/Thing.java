package application.models;

import java.util.Objects;

public class Thing extends Buyable {
    protected String name;
    protected String type;

    public Thing(String name, String type, int price) {
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public Thing(String name, String type) {
        this(name, type, 0);
    }

    public Thing() {
        this("", "", 0);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Thing name(String name) {
        this.name = name;
        return this;
    }

    public Thing type(String type) {
        this.type = type;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Thing)) {
            return false;
        }
        Thing thing = (Thing) o;
        return Objects.equals(name, thing.name) && Objects.equals(type, thing.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type);
    }

    @Override
    public String toString() {
        return getName() + " - " + getType();
    }

}