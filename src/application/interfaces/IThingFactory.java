package application.interfaces;

import java.util.List;

import application.models.Thing;

public interface IThingFactory {
    public List<Thing> create();
}