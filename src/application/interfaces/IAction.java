package application.interfaces;

import java.util.List;

import application.models.Person;
import application.models.Thing;

public interface IAction {
    public void doAction(List<Person> persons, List<Thing> things);

    public String toString();
}