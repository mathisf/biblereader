package application.interfaces;

import application.platform.Loader;

public interface IApplication {
    public void run();

    public void setLoader(Loader loader);
}